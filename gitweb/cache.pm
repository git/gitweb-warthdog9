# gitweb - simple web interface to track changes in git repositories
#
# (C) 2006, John 'Warthog9' Hawley <warthog19@eaglescrag.net>
#
# This program is licensed under the GPLv2

#
# Gitweb caching engine
#

#package gitweb::cache;
#use strict;
#use warnings;

#my $VERSION = "0.00";

use File::Copy;

sub cache_fetch {
	my ($action) = @_;
	my $cacheTime = 0;
	
	# Deal with cache being disabled
	if( $cache_enable == 0 ){
		undef $backgroundCache;
		$fullhashpath = \$nocachedata;
		$fullhashbinpath = \$nocachedatabin;
	}elsif( $cache_enable == 1 ){
		#obvious we are using file based caching
	
		#print "Action: $action\n";
		if(! -d $cachedir){
			print "*** Warning ***: Caching enabled but cache directory does not exsist.  ($cachedir)\n";
			mkdir ("cache", 0665) || die "Cannot create cache dir - you will need to manually create";
			print "Cache directory created successfully\n";
		}
	
		#our $local_url =  "". $ENV{'HTTP_HOST'} ." + ". $ENV{'PATH_INFO'} ." + ". $ENV{'QUERY_STRING'} ."\n";
		#print "URL: $local_url\n";
		our $full_url = "$my_url?". $ENV{'QUERY_STRING'};
		our $urlhash = md5_hex($full_url);
		our $fullhashdir = "$cachedir/". substr( $urlhash, 0, 2) ."/";

		#if( $] < 5.010000 ) {
			eval { mkpath( $fullhashdir, 0, 0777 ) };
			if ($@) {
				die_error(500, "Internal Server Error", "Could not create cache directory: $@");
			}
		#} else {
		#	my $numdirs = make_path( $fullhashdir, { mode => 0777, error => \my $mkdirerr, } ); # This is only used with the newer File::Path
		#	if( @$mkdirerr ){
		#		my $mkdirerrmsg = "";
		#		for my $diag (@$mkdirerr) {
		#			my ($file, $message) = %$diag;
		#			if($file eq '' ){
		#				$mkdirerrmsg .= "general error: $message\n";
		#			}else{
		#				$mkdirerrmsg .= "problem unlinking $file: $message\n";
		#			}
		#		}
		#		die_error(500, "Internal Server Error", "Could not create cache directory | $mkdirerrmsg");
		#	}
		#} # End figuring out File::Path usage
		$fullhashpath = "$fullhashdir/". substr( $urlhash, 2 );
		$fullhashbinpath = "$fullhashpath.bin.wt";
		$fullhashbinpathfinal = "$fullhashpath.bin";
	} # done dealing with cache enabled / disabled

	if(! -e "$fullhashpath" ){
		if( ! $cache_enable || ! $cacheDoFork || ! defined(my $childPid = fork()) ){
			cacheUpdate($action,0);
			cacheDisplay($action);
		} elsif ( $childPid == 0 ){
			#run the updater
			cacheUpdate($action,1);
		}else{
			cacheWaitForUpdate($action);
		}
	}else{
		#if cache is out dated, update
		#else displayCache();
		open(cacheFile, '<', "$fullhashpath");
		stat(cacheFile);
		close(cacheFile);
		$cacheTime = get_loadavg() * 60;
		if( $cacheTime > $maxCacheTime ){
			$cacheTime = $maxCacheTime;
		}
		if( $cacheTime < $minCacheTime ){
			$cacheTime = $minCacheTime;
		}
		if( (stat(_))[9] < (time - $cacheTime) || (stat(_))[7] == 0 ){
			if( ! $cacheDoFork || ! defined(my $childPid = fork()) ){
				cacheUpdate($action,0);
				cacheDisplay($action);
			} elsif ( $childPid == 0 ){
				#run the updater
				#print "Running updater\n";
				cacheUpdate($action,1);
			}else{
				#print "Waiting for update\n";
				cacheWaitForUpdate($action);
			}
		} else {
			cacheDisplay($action);
		}
		
	
	}
	
	#
	# If all of the caching failes - lets go ahead and press on without it and fall back to 'default' 
	# non-caching behavior.  This is the softest of the failure conditions.
	#
	#$actions{$action}->();
}

sub cacheUpdate {
	my ($action,$areForked) = @_;
	my $lockingStatus;
	my $fileData = "";
	
	if($backgroundCache){
		open(cacheFileBG, '>:utf8', "$fullhashpath.bg");
		my $lockStatBG = flock(cacheFileBG,LOCK_EX|LOCK_NB);
		
		$lockStatus = $lockStatBG;
	}else{
		open(cacheFile, '>:utf8', "$fullhashpath") if ($cache_enable > 0);
		open(cacheFile, '>:utf8', \$fullhashpath) if ($cache_enable == 0);
		my $lockStat = flock(cacheFile,LOCK_EX|LOCK_NB);
		
		$lockStatus = $lockStat;
	}
	#print "lock status: $lockStat\n";
	
	
	if ($cache_enable != 0 && ! $lockStatus ){
		if ( $areForked ){
			exit(0);
		}else{
			return;
		}
	}
	
	if(
		$action eq "snapshot"
		||
		$action eq "blob_plain"
	){
		my $openstat = open(cacheFileBinWT, '>>:utf8', "$fullhashbinpath");
		my $lockStatBin = flock(cacheFileBinWT,LOCK_EX|LOCK_NB);

	}
	
	$fileData = "". $actions{$action}->() ."";

	if($backgroundCache){
		open(cacheFile, '>:utf8', "$fullhashpath");
		$lockStat = flock(cacheFile,LOCK_EX);

		if (! $lockStat ){
			if ( $areForked ){
				exit(0);
			}else{
				return;
			}
		}

	}

	if(
		$action eq "snapshot"
		||
		$action eq "blob_plain"
	){
		my $openstat = open(cacheFileBinFINAL, '>:utf8', "$fullhashbinpathfinal");
		$lockStatBIN = flock(cacheFileBinFINAL,LOCK_EX);
	
		if (! $lockStatBIN ){
			if ( $areForked ){
				exit(0);
			}else{
				return;
			}
		}
	}
	
	local $/ = undef;
	$|++;
	print cacheFile "$fileData";
	$|--;
	if(
		$action eq "snapshot"
		||
		$action eq "blob_plain"
	){
		move("$fullhashbinpath", "$fullhashbinpathfinal") or die "Binary Cache file could not be updated: $!";

		flock(cacheFileBinFINAL,LOCK_UN);
		close(cacheFileBinFINAL);

		flock(cacheFileBinWT,LOCK_UN);
		close(cacheFileBinWT);
	}
	
	flock(cacheFile,LOCK_UN);
	close(cacheFile);
	
	if($backgroundCache){
		flock(cacheFileBG,LOCK_UN);
		close(cacheFileBG);
	}
	
	if ( $areForked ){
		exit(0);
	} else {
		return;
	}
}


sub cacheWaitForUpdate {
	my ($action) = @_;
	my $x = 0;
	my $max = 10;
	my $lockStat = 0;

	if( $backgroundCache ){
		if( -e "$fullhashpath" ){
			open(cacheFile, '<:utf8', "$fullhashpath") if ($cache_enable > 0);
			open(cacheFile, '<:utf8', \$fullhashpath) if ($cache_enable == 0);
			$lockStat = flock(cacheFile,LOCK_SH|LOCK_NB);
			stat(cacheFile);
			close(cacheFile);

			if( $lockStat && ( (stat(_))[9] > (time - $maxCacheLife) ) ){
				cacheDisplay($action);
				return;
			}
		}
	}
	
	#our $headerRefresh = 1;
	#print git_header_html();
	
	if(
		$action eq "atom"
		||
		$action eq "rss"
		||
		$action eq "opml"
	){
		do {
			sleep 2 if $x > 0;
			open(cacheFile, '<:utf8', "$fullhashpath") if ($cache_enable > 0);
			open(cacheFile, '<:utf8', \$fullhashpath) if ($cache_enable == 0);
			$lockStat = flock(cacheFile,LOCK_SH|LOCK_NB);
			close(cacheFile);
			$x++;
			$combinedLockStat = $lockStat;
		} while ((! $combinedLockStat) && ($x < $max));

		if( $x != $max ){
			cacheDisplay($action);
		}
		return;
	}
	
	$| = 1;
	
	print $::cgi->header(
				-type=>'text/html',
				-charset => 'utf-8',
				-status=> 200,
				-expires => 'now',
				# HTTP/1.0
				-Pragma => 'no-cache',
				# HTTP/1.1
				-Cache_Control => join(
							', ',
							qw(
								private
								no-cache
								no-store
								must-revalidate
								max-age=0
								pre-check=0
								post-check=0
							)
						)
				);

	print <<EOF;
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www/w3.porg/TR/html4/strict.dtd">
<!-- git web interface version $version, (C) 2005-2006, Kay Sievers <kay.sievers\@vrfy.org>, Christian Gierke -->
<!-- git core binaries version $git_version -->
<head>
<meta http-equiv="content-type" content="$content_type; charset=utf-8"/>
<meta name="generator" content="gitweb/$version git/$git_version"/>
<meta name="robots" content="index, nofollow"/>
<meta http-equiv="refresh" content="0"/>
<title>$title</title>
</head>
<body>
EOF
	 
	print "Generating..";
	do {
		print ".";
		sleep 2 if $x > 0;
		open(cacheFile, '<:utf8', "$fullhashpath") if ($cache_enable > 0);
		open(cacheFile, '<:utf8', \$fullhashpath) if ($cache_enable == 0);
		$lockStat = flock(cacheFile,LOCK_SH|LOCK_NB);
		close(cacheFile);
		#$lockStatbg = flock(cacheFilebg,LOCK_SH|LOCK_NB) if $backgroundCache;
		#print "Lock State: $lockStat\n";
		#if( ! $lockStat ){
		#	print "Error: ". $! ."\n";
		#}
		$x++;
		$combinedLockStat = $lockStat;
		#$combinedLockStat = $lockStat & $lockStatbg if $backgroundCache;
	} while ((! $combinedLockStat) && ($x < $max));
	#} while ($x < $max);
	#close(cacheFile);
	#close(cacheFilebg) if $backgroundCache;
	#print git_footer_html();
	print <<EOF;
</body>
</html>
EOF
	#exit(0);
	return;
	#cacheDisplay();
}

sub cacheDisplay {
	local $/ = undef;
	$|++;

	my ($action) = @_;
	open(cacheFile, '<:utf8', "$fullhashpath") if ($cache_enable > 0);
	open(cacheFile, '<:utf8', \$fullhashpath) if ($cache_enable == 0);
	$lockStat = flock(cacheFile,LOCK_SH|LOCK_NB);

	if ($cache_enable != 0 && ! $lockStat ){
		close(cacheFile);
		cacheWaitForUpdate($action);
	}
	
	if(
		(
			$action eq "snapshot"
			||
			$action eq "blob_plain"
		)
		&&
		$cache_enable > 0
		
	){
		my $openstat = open(cacheFileBin, '<', "$fullhashbinpathfinal");
		$lockStatBIN = flock(cacheFileBin,LOCK_SH|LOCK_NB);
		if ($cache_enable != 0 && ! $lockStatBIN ){
			system ("echo 'cacheDisplay - bailing due to binary lock failure' >> /tmp/gitweb.log");
			close(cacheFile);
			close(cacheFileBin);
			cacheWaitForUpdate($action);
		}

		my $binfilesize = -s "$fullhashbinpathfinal";
		print "Content-Length: $binfilesize\r\n";
	}
	while( <cacheFile> ){
		print $_;
	}
	if(
		$action eq "snapshot"
		||
		$action eq "blob_plain"
	){
		#open(cacheFileBin, '<', "$fullhashbinpath");	#this seems superfluous if I'm opening it above for the size and locking
		binmode STDOUT, ':raw';
		print <cacheFileBin>;
		binmode STDOUT, ':utf8'; # as set at the beginning of gitweb.cgi
		close(cacheFileBin);
	}
	close(cacheFile);
	$|--;
}

#1;
